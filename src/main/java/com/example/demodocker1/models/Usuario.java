package com.example.demodocker1.models;

public abstract class Usuario {

    private String nombre;
    private String rut;
    private String clave;

    public Usuario(String nombre, String rut, String clave) {
        this.nombre = nombre;
        this.rut = rut;
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRut() {
        return rut;
    }

    public String getClave() {
        return clave;
    }
}
