package com.example.demodocker1.models;

public class Reponedor extends Usuario {

    private String seccion;

    public Reponedor(String nombre, String rut, String clave, String seccion) {
        super(nombre, rut, clave);
        this.seccion = seccion;
    }

    public String getSeccion() {
        return seccion;
    }
}
