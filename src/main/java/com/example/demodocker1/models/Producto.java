package com.example.demodocker1.models;

public class Producto {

    private String nombre;
    private String seccion;
    private int stock;
    private int precio;
    private String ubicacion;

    public Producto(String nombre, String seccion, int stock, int precio, String ubicacion) {
        this.nombre = nombre;
        this.seccion = seccion;
        this.stock = stock;
        this.precio = precio;
        this.ubicacion = ubicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getSeccion() {
        return seccion;
    }

    public int getStock() {
        return stock;
    }

    public int getPrecio() {
        return precio;
    }

    public String getUbicacion() {
        return ubicacion;
    }
}
