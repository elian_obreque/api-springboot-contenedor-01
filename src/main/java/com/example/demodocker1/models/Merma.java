package com.example.demodocker1.models;

public class Merma {

    private String nombre;
    private String motivo;
    private int cantidad;

    public Merma(String nombre, String motivo, int cantidad) {
        this.nombre = nombre;
        this.motivo = motivo;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMotivo() {
        return motivo;
    }

    public int getCantidad() {
        return cantidad;
    }
}
