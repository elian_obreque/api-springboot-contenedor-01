package com.example.demodocker1.models;

public class Cajero extends Usuario {

    private int ventas;

    public Cajero(String nombre, String rut, String clave, int ventas) {
        super(nombre, rut, clave);
        this.ventas = ventas;
    }

    public int getVentas() {
        return ventas;
    }
}
