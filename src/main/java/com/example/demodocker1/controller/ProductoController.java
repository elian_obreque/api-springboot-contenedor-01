package com.example.demodocker1.controller;

import com.example.demodocker1.models.Producto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductoController {

    private List<Producto> productosList = new ArrayList<>();

    @GetMapping("/addProducts")
    public String agregarProductos() {
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));
        productosList.add(new Producto("Pisco Mistral 1lt", "Bebidas", 25, 6700, "Pasillo D"));

        return "[Productos agregados exitosamente]";
    }

    @GetMapping("/listProducts")
    public List<Producto> getProductos() {
        return productosList;
    }
}
