package com.example.demodocker1.controller;

import com.example.demodocker1.models.Cajero;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/cashiers")
public class CajeroController {

    private List<Cajero> cajerosList = new ArrayList<>();

    @GetMapping("/addCashiers")
    public String agregarCajeros() {
        cajerosList.add(new Cajero("Juan", "20345678-7", "progrA466", 24000));
        cajerosList.add(new Cajero("Juan", "20345678-7", "progrA466", 24000));
        cajerosList.add(new Cajero("Juan", "20345678-7", "progrA466", 24000));
        cajerosList.add(new Cajero("Juan", "20345678-7", "progrA466", 24000));
        cajerosList.add(new Cajero("Juan", "20345678-7", "progrA466", 24000));

        return "[Cajeros agregados exitosamente]";
    }

    @GetMapping("/listCashiers")
    public List<Cajero> getCajeros() {
        return cajerosList;
    }
}
