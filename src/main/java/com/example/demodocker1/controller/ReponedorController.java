package com.example.demodocker1.controller;

import com.example.demodocker1.models.Reponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/stockers")
public class ReponedorController {

    private List<Reponedor> reponedoresList = new ArrayList<>();

    @GetMapping("/addStockers")
    public String agregarReponedores() {
        reponedoresList.add(new Reponedor("Elian", "20414844-9", "clave123", "Pasillo D"));
        reponedoresList.add(new Reponedor("Elian", "20414844-9", "clave123", "Pasillo D"));
        reponedoresList.add(new Reponedor("Elian", "20414844-9", "clave123", "Pasillo D"));

        return "[Reponedores agregados exitosamente]";
    }

    @GetMapping("/listStockers")
    public List<Reponedor> getReponedores() {
        return reponedoresList;
    }
}
