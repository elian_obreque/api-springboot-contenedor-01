package com.example.demodocker1.controller;

import com.example.demodocker1.models.Merma;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/mermas")
public class MermaController {

    private List<Merma> mermasList = new ArrayList<>();

    @GetMapping("/addMermas")
    public String agregarMermas() {
        mermasList.add(new Merma("Coca-cola 3 lts", "Negligencia", 10));
        mermasList.add(new Merma("Coca-cola 3 lts", "Negligencia", 10));
        mermasList.add(new Merma("Coca-cola 3 lts", "Negligencia", 10));

        return "[Mermas agregadas exitosamente]";
    }

    @GetMapping("/listMermas")
    public List<Merma> getMermas() {
        return mermasList;
    }
}
