FROM openjdk:17

VOLUME /tmp

EXPOSE 8080

COPY target/demo-docker-1-0.0.1-SNAPSHOT.jar demo-container.jar

ENTRYPOINT ["java", "-jar", "demo-container.jar"]

#docker build -t actividad-docker-1 .
#docker run -p 8080:8080 -d actividad-docker-1